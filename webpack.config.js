const path = require('path');
const webpack = require('webpack');
const autoprefixer = require('autoprefixer');
const AssetsPlugin = require('assets-webpack-plugin');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const ImageminPlugin = require('imagemin-webpack-plugin').default;
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const SimpleProgressWebpackPlugin = require("simple-progress-webpack-plugin");

const BUILD = path.resolve(__dirname, 'building');
const SRC = path.resolve(__dirname, 'src');
const isDevelopment = process.env.NODE_ENV === 'development';

module.exports = {
    entry: {
        main: [SRC + '/js/index.js'],
        send: [SRC + '/js/send.js']
    },
    output: {
        filename: isDevelopment ? '[name].js' : 'js/[name].[chunkhash:8].js',
        path: BUILD
    },
    devServer: {
        overlay: true
    },
    resolve: {
        extensions: ['.js', '.scss', '.sass', '.css', '.svg', '.html'],
        modules: ['node_modules']
    },
    module: {
        rules: [
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                include: SRC + '/img',
                loader: isDevelopment ? 'url-loader?limit=30000&name=img/[name].[ext]' : 'url-loader?limit=30000&name=img/[name].[hash:8].[ext]'
            },
            {
                enforce: 'pre',
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'eslint-loader',
                options: {
                    formatter: require('eslint/lib/formatters/codeframe'),
                    emitError: true,
                    emitWarning: true,
                    eslintrc: false
                }
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: ['babel-loader']
            },
            {
                test: /\.(sass|scss)$/,
                use: ExtractTextPlugin.extract({
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                minimize: true,
                                sourceMap: true,
                                url: false
                            }
                        },
                        {
                            loader: 'postcss-loader',
                            options: {
                                plugins: [
                                    autoprefixer({
                                        browsers:[
                                            '> 1%',
                                            'iOS 8'
                                        ]
                                    })
                                ],
                                sourceMap: true
                            }
                        },
                        {
                            loader: 'sass-loader',
                            options: {
                                sourceMap: true
                            }
                        }
                    ]
                })
            }
        ]
    },
    devtool: isDevelopment ? 'source-map' : false,
    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery'
        }),
        new CleanWebpackPlugin([BUILD], {
            root: '/',
            dry: false,
            verbose: false,
            watch: false
        }),
        new ImageminPlugin({
            disable: isDevelopment,
            test: /\.(jpe?g|png|gif)$/i,
            jpegtran: {  progressive: true },
            pngquant: {
                quality: '70-100'
            }
        }),
        new ExtractTextPlugin(isDevelopment ? '[name].css' : 'css/[name].[chunkhash:8].css'),
        new HtmlWebPackPlugin({
            template: SRC + '/index.html',
            filename: 'index.html',
            chunks: ['main']
        }),
        new HtmlWebPackPlugin({
            template: SRC + '/page/send.html',
            filename: 'page/send.html',
            chunks: ['send']
        }),
        new AssetsPlugin({
            keepInMemory: isDevelopment,
            filename: isDevelopment ? '/assets.json' : '/building/assets.json'
        }),
        new SimpleProgressWebpackPlugin({
            format: "minimal"
        }),
    ]
};